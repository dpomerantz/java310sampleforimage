package com.example;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    final double TOLERANCE = .0000001;

    // DO NOT PUT A VARIABLE LIKE THIS IN A REAL TEST!
    private static Rectangle r = new Rectangle(2,3);    

    @Test
    public void testArea()
    {
        assertEquals(6, r.getArea(), TOLERANCE);
        r.setLength(10);
    }

    @Test
    public void testPerimeter() {
        assertEquals(26, r.getPerimeter(), TOLERANCE);
    }
}
