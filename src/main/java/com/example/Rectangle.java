package com.example;

/**
 * Hello world!
 *
 */
public class Rectangle 
{
    private double length;
    public Rectangle(double length, double width) {
        this.length = length;
        this.width = width;
    }
    public double getLength() {
        return length;
    }
    public double getWidth() {
        return width;
    }
    public void setLength(double length) {
        this.length = length;
    }
    public void setWidth(double width) {
        this.width = width;
    }
    private double width;

    public double getArea() { return this.width * this.length;} 
    public double getPerimeter() { return 2 * (this.width + this.length); }
    
}
