package com.example;

public class IsNumber {
    public static boolean isNumber(String s) {

        boolean isValid = true;
        // check for negatives first!
        if (s.charAt(0) == '-') {
            for (int i = 1; i < s.length(); i++) {
                if (Character.isDigit(s.charAt(i))) {
                    // do nothing, move to the next character
                } 
                else {
                    isValid = false; // it's definitely invalid!
                }
            }
        } else {
            for (int i = 1; i < s.length(); i++) {
                if (Character.isDigit(s.charAt(i))) {
                    // do nothing, move to the next character
                } 
                else {
                    isValid = false; // it's definitely invalid!
                }
            }
        }
        return isValid;
    }
}
